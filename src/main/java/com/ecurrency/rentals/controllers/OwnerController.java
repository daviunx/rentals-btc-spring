package com.ecurrency.rentals.controllers;

import com.ecurrency.rentals.domain.Rental;
import com.ecurrency.rentals.domain.Owner;
import com.ecurrency.rentals.repository.OwnerRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

/**
 * Created by pepito on 4/22/15.
 */
@Component
@Path("/owner")
@Produces(MediaType.APPLICATION_JSON)
public class OwnerController {
    @Inject
    private OwnerRepository ownerRepository;

    @Context
    private UriInfo uriInfo;

    @GET
    public Page<Owner> findAll(
            @QueryParam("page") @DefaultValue("0") int page,
            @QueryParam("size") @DefaultValue("20") int size,
            @QueryParam("sort") @DefaultValue("firstName") List<String> sort,
            @QueryParam("direction") @DefaultValue("asc") String direction) {

        return ownerRepository.findAll(
                new PageRequest(
                        page,
                        size,
                        Sort.Direction.fromString(direction),
                        sort.toArray(new String[0])
                )
        );
    }

    @GET
    @Path("{id}")
    public Owner findOne(@PathParam("id") Long id) {
        return ownerRepository.findOne(id);
    }

    @POST
    public Response save(Owner owner) {
        owner = ownerRepository.save(owner);

        URI location = uriInfo.getAbsolutePathBuilder()
                .path("{id}")
                .resolveTemplate("id", owner.getId())
                .build();

        return Response.created(location).build();
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") Long id) {
        ownerRepository.delete(id);
        return Response.accepted().build();
    }

}
