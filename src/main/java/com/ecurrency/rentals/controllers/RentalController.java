package com.ecurrency.rentals.controllers;

import com.ecurrency.rentals.domain.Rental;
import com.ecurrency.rentals.repository.RentalRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

/**
 * Created by pepito on 4/22/15.
 */
@Component
@Path("/rental")
@Produces(MediaType.APPLICATION_JSON)
public class RentalController {

    @Inject
    private RentalRepository rentalRepository;

    @Context
    private UriInfo uriInfo;

    @GET
    public Page<Rental> findAll(
            @QueryParam("page") @DefaultValue("0") int page,
            @QueryParam("size") @DefaultValue("20") int size,
            @QueryParam("sort") @DefaultValue("name") List<String> sort,
            @QueryParam("direction") @DefaultValue("asc") String direction) {

        return rentalRepository.findAll(
                new PageRequest(
                        page,
                        size,
                        Sort.Direction.fromString(direction),
                        sort.toArray(new String[0])
                )
        );
    }

    @GET
    @Path("{id}")
    public Rental findOne(@PathParam("id") Long id) {
        return rentalRepository.findOne(id);
    }

    @POST
    public Response save(Rental rental) {
        rental = rentalRepository.save(rental);

        URI location = uriInfo.getAbsolutePathBuilder()
                .path("{id}")
                .resolveTemplate("id", rental.getId())
                .build();

        return Response.created(location).build();
    }


    @PUT
    @Path("{id}")
    public Response update(@PathParam("id") Long id, Rental rental) {
        rental.setId(id);
        rentalRepository.save(rental);
        return Response.accepted().build();
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") Long id) {
        rentalRepository.delete(id);
        return Response.accepted().build();
    }

}
