package com.ecurrency.rentals.controllers;

import com.ecurrency.rentals.domain.Attribute;
import com.ecurrency.rentals.domain.Owner;
import com.ecurrency.rentals.repository.AttributeRepository;
import com.ecurrency.rentals.repository.OwnerRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

/**
 * Created by pepito on 4/23/15.
 */
public class AttributeController {

    @Inject
    private AttributeRepository attributeRepository;

    @Context
    private UriInfo uriInfo;

    @GET
    @Path("{id}")
    public Attribute findOne(@PathParam("id") Long id) {
        return attributeRepository.findOne(id);
    }

    @POST
    public Response save(Attribute attribute) {
        attribute = attributeRepository.save(attribute);

        URI location = uriInfo.getAbsolutePathBuilder()
                .path("{id}")
                .resolveTemplate("id", attribute.getId())
                .build();

        return Response.created(location).build();
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") Long id) {
        attributeRepository.delete(id);
        return Response.accepted().build();
    }
}
