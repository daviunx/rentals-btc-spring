package com.ecurrency.rentals.domain;

import javax.persistence.*;

/**
 * Created by pepito on 4/23/15.
 */
@Entity
@Table(name = "rental_attributes")
public class Attribute extends AbstractEntity {

    @Column(name = "name")
    private String key;

    private String value;

    @ManyToOne(optional=false)
    @JoinColumn(name="rental_id", nullable=false, referencedColumnName = "id")
    private Rental rental;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Rental getRental() {
        return rental;
    }

    public void setRental(Rental rental) {
        this.rental = rental;
    }
}
