package com.ecurrency.rentals.domain;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by pepito on 4/22/15.
 */

@Entity
@Table(name = "rental")
public class Rental extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    private String name = null;
    private String description = null;
    private Integer surfaceArea = 20;

    @ManyToOne(optional=false)
    @JoinColumn(name="owner_id", nullable=false, referencedColumnName = "id")
    private Owner owner;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rental", fetch=FetchType.EAGER)
    private Set<Attribute> attributes;

    private Date created = new Date();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSurfaceArea() {
        return surfaceArea;
    }

    public void setSurfaceArea(Integer surfaceArea) {
        this.surfaceArea = surfaceArea;
    }

    @JsonBackReference
    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Set<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(Set<Attribute> attributes) {
        this.attributes = attributes;
    }
}
