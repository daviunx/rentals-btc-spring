package com.ecurrency.rentals.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    /*
     * Method for update entities. Maybe is not the best solution
     */
    public void setId(Long id) {
        this.id = id;
    }
}
