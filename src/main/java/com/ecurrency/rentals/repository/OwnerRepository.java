package com.ecurrency.rentals.repository;

import com.ecurrency.rentals.domain.Owner;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by pepito on 4/22/15.
 */
public interface OwnerRepository  extends PagingAndSortingRepository<Owner, Long> {
}
