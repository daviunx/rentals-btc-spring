package com.ecurrency.rentals.repository;

import com.ecurrency.rentals.domain.Attribute;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by pepito on 4/23/15.
 */
public interface AttributeRepository extends PagingAndSortingRepository<Attribute, Long> {


}
