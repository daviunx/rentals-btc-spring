package com.ecurrency.rentals.repository;

/**
 * Created by pepito on 4/22/15.
 */

import com.ecurrency.rentals.domain.Rental;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RentalRepository extends PagingAndSortingRepository<Rental, Long>  {

}
