package com.ecurrency.rentals.ws;

import com.ecurrency.rentals.controllers.AttributeController;
import com.ecurrency.rentals.controllers.OwnerController;
import com.ecurrency.rentals.controllers.RentalController;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(AttributeController.class);
        register(OwnerController.class);
        register(RentalController.class);
    }

}

