# Docker
*Docker and Fig configurations for running external applications*

This scripts rise up different applications to have a local development environment

## Installation
1. Install Docker version 1.3 or greater https://docs.docker.com/installation/
2. Install Fig http://www.fig.sh/install.html
3. Make sure is running all docker services
3. Launch distributed applications with Fig
*First time take a long time*
```
    cd docker/
	fig up
```

## Used distributed applications
### MySql
- password: *root*
